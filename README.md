# paywright-vite-test

Repo for trying to reproduce issue with playwright testing in GitLab/docker image - but were never able to get it to fail on the SaaS runners.

Link to issue: https://github.com/microsoft/playwright/issues/27313

The solution in our actual repo was to add this `before_script`

```yml
  image: mcr.microsoft.com/playwright:v1.38.1-jammy
  before_script:
    - apt-get remove -y libsoup2.4
```
