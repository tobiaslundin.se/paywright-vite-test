import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import mkcert from "vite-plugin-mkcert";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    https: true,
    port: 3001,
  },
  preview: {
    https: true,
    port: 3001,
  },
  plugins: [mkcert(), react()],
});
