import { BrowserContext } from 'playwright/test';

import { apiGwSettings, apiGwSubscriptions, apiLayoutPages } from './fixtures';

/**
 * Mock common calls and abort calls for external services to speed up tests
 * Docs at https://playwright.dev/docs/mock
 * */
export const mockCommonApiCalls = async (context: BrowserContext) => {
  // Abort calls for LaunchDarkly
  await context.route(/launchdarkly\.com/, route => route.abort());
  // Abort call for sts context
  await context.route(/\/context\/check/, route => route.abort());
  // abort calls for images and fonts
  await context.route(/\/static\.(uat\.)?rikstv\.no/, route => route.abort());
  // abort calls for ga and cast-lib
  await context.route(/\/stm\.strim\.no/, route => route.abort());
  await context.route(/\/www\.gstatic\.com/, route => route.abort());

  // Mock static responses with fixtures, update fixtures by running `npm run update`
  await context.route(/\/pages$/, route => route.fulfill({ json: apiLayoutPages }));
  await context.route(/\/settings$/, route => route.fulfill({ json: apiGwSettings }));
  await context.route(/\/subscriptions$/, route => route.fulfill({ json: apiGwSubscriptions }));
};
