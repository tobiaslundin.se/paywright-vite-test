import apiGwSettings from './gateway/settings.json' assert { type: 'json' };
import apiGwSubscriptions from './gateway/subscriptions.json' assert { type: 'json' };
import apiLayoutPages from './layout/pages.json' assert { type: 'json' };

// Convenience export from here to avoid import assertions in other files
export { apiGwSettings, apiGwSubscriptions, apiLayoutPages };
