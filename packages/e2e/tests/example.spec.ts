import { test, expect } from "@playwright/test";
import { mockCommonApiCalls } from "../mocks/api";

// for chomium to work with custom cert on https
test.use({ ignoreHTTPSErrors: true });

test.beforeEach(async ({ context }) => {
  await mockCommonApiCalls(context);
  await context.addInitScript({
    content: 'globalThis.process = { env: { MODE: "test" } };',
  });
});

test("has title", async ({ page }) => {
  await page.goto("/");

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Vite \+ React \+ TS/);
});

test("counter click", async ({ page }) => {
  await page.goto("/");

  await page.getByRole("button").click();

  await expect(page.getByText("count is 1")).toBeVisible();
});
